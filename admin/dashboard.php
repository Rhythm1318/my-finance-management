<?php 
include("inc/connect.php");
include("inc/chkAuth.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>AR finance Admin - dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">AR Finance</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <!--<li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>-->
       
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <!--<li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>-->
            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
	<?php
	include("inc/menu.php");
	?>
    <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i> <?php

      echo "Welcome, ".$_SESSION['name']."<br>";?></h1>

      <?

      $sql="select last_activity,login_ip from admin where admin_id=".$_SESSION['admin_id'];
      $rs=mysqli_query($conn,$sql);
      $cnt=mysqli_num_rows($rs);

          while($row=mysqli_fetch_array($rs))
          { 
            //echo "<div style='text-align:right'>";
            echo "<h5>Last Login: ".$row['last_activity'];
            echo " from IP: ".$row['login_ip']."</h5>";
          }
?>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        </ul>
		
      </div>

      
	  
      <div class="row">
        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">

              
			<?php 
			$sql="select COUNT(user_id) from users";



$rs=mysqli_query($conn,$sql);
$cnt=mysqli_num_rows($rs);
while($row=mysqli_fetch_array($rs))
{ 
  ?>
              <h4>Number of Users</h4>
              <p><b><?php echo "<a href=mng_user.php?countusers=".$row['COUNT(user_id)'].'>'.$row['COUNT(user_id)']."</a>";?></b></p>
            </div>
          </div>
        </div>
<?php
}
?>


        <div class="col-md-6 col-lg-3">
          <div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-o-up fa-3x"></i>
            <div class="info">
			<?php

$sql="SELECT * FROM `society_account` ORDER BY `society_account`.`trans_id` DESC LIMIT 1";

$rs=mysqli_query($conn,$sql);

$cnt=mysqli_num_rows($rs);

  

while($row=mysqli_fetch_array($rs))

  { ?>
              <h4>Society Account Balance</h4>
              <p><b><?php echo round($row['cur_bal']);?></b></p>
            </div>
          </div>
        </div>

  <?php
  }
  ?>
  
        <div class="col-md-6 col-lg-3">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-files-o fa-3x"></i>
            <div class="info">
            <?php 
            $sql="select COUNT(acc_id) from user_account where acc_type=1 AND status=1";
            $rs=mysqli_query($conn,$sql);
            while($row=mysqli_fetch_array($rs))

                  {
                    ?>
                                <h4>Total FD Accounts</h4>
                                <p><b><?php echo $row['COUNT(acc_id)'];?></b></p>
                                </div>
                            </div>
                          </div>
                              
                  <?php
                  }?>


        <div class="col-md-6 col-lg-3">
          <div class="widget-small warning coloured-icon"><i class="icon fa fa-files-o fa-3x"></i>
            <div class="info">
<?php
            $sql="SELECT SUM(fd_amt) AS 'total fd_amt' FROM user_account";

            $rs=mysqli_query($conn,$sql);
            $cnt=mysqli_num_rows($rs);
            while($row=mysqli_fetch_array($rs))

                  {
                    ?>


                                <h4>Total FD Deposit</h4>
                                <p><b><?php echo "<a href=userfdacc.php?totalfd=".round($row['total fd_amt']).'>'.round($row['total fd_amt'])."</a>";?></b></p>

                                </div>
                            </div>
                          </div>
                  <?php
                  }
                  ?>




        <div class="col-md-6 col-lg-3">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
            <?php 
            $sql="select COUNT(acc_id) from user_account where acc_type=2 AND status=1";

            $rs=mysqli_query($conn,$sql);
            $cnt=mysqli_num_rows($rs);
            while($row=mysqli_fetch_array($rs))

                {
                  ?>
                              <h4>Total RD Accounts</h4>
                              <p><b><?php echo $row['COUNT(acc_id)'];?></b></p>
                            </div>
                          </div>
                        </div>
                <?php
                }
                ?>


                <div class="col-md-6 col-lg-3">
          <div class="widget-small danger coloured-icon"><i class="icon fa fa-star fa-3x"></i>
            <div class="info">
            <?php 
            $sql="SELECT SUM(cur_bal) AS 'total rd_amt' FROM user_account";

            $rs=mysqli_query($conn,$sql);
            $cnt=mysqli_num_rows($rs);
            while($row=mysqli_fetch_array($rs))

                {
                  ?>
                              <h4>Total RD Deposit</h4>
                              <p><b><?php echo "<a href=useracc.php?totalrd=".round($row['total rd_amt']).'>'.round($row['total rd_amt'])."</a>";?></b></p>
                            </div>
                          </div>
                        </div>
                <?php
                }
                ?>


        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
			<?php 
			$sql="SELECT SUM(loan_amt) AS 'total loan_amt' FROM user_account";

      $rs=mysqli_query($conn,$sql);
      $cnt=mysqli_num_rows($rs);

        while($row=mysqli_fetch_array($rs))

            {
            			?>
                          <h4>Total Loan Amount</h4>
                          <p><b><?php echo "<a href=userloanacc.php?totalloan=".$row['total loan_amt'].'>'.$row['total loan_amt']."</a>";?></b></p>
                        </div>
                      </div>
                    </div>
            <?php
            }
            ?>


        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
      <?php 
      $sql="select COUNT(app_id) from loan_application where status=1";
      $rs=mysqli_query($conn,$sql);
      while($row=mysqli_fetch_array($rs))
        {
                          ?>
                                  <h4>Approved Loan Requests</h4>
                                  <p><b><?php echo $row['COUNT(app_id)'];?></b></p>
                                </div>
                              </div>
                            </div>
        <?php
        }
        ?>


        <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
            <div class="info">
      <?php 
      $sql="select COUNT(app_id) from loan_application where status=0";
      $rs=mysqli_query($conn,$sql);
      while($row=mysqli_fetch_array($rs))
        {
                          ?>
                                  <h4> Rejected Loan Requests</h4>
                                  <p><b><?php echo $row['COUNT(app_id)'];?></b></p>
                                </div>
                              </div>
                            </div>
        <?php
        }
        ?>


     <div class="col-md-6 col-lg-3">
    <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
        <div class="info">
      <?php 
      $sql="select COUNT(app_id) from loan_application where status=2";
      $rs=mysqli_query($conn,$sql);
      
      while($row=mysqli_fetch_array($rs))
        {
         ?>
                                  <h4> Pending Loan Requests</h4>
                                  <p><b><?php echo $row['COUNT(app_id)'];?></b></p>
                                </div>
                              </div>
                            </div>
        <?php
        }
        ?>
            

   




        
    
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <script type="text/javascript" src="js/plugins/chart.js"></script>
    <script type="text/javascript">
      var data = {
      	labels: ["January", "February", "March", "April", "May"],
      	datasets: [
      		{
      			label: "My First dataset",
      			fillColor: "rgba(220,220,220,0.2)",
      			strokeColor: "rgba(220,220,220,1)",
      			pointColor: "rgba(220,220,220,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(220,220,220,1)",
      			data: [65, 59, 80, 81, 56]
      		},
      		{
      			label: "My Second dataset",
      			fillColor: "rgba(151,187,205,0.2)",
      			strokeColor: "rgba(151,187,205,1)",
      			pointColor: "rgba(151,187,205,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(151,187,205,1)",
      			data: [28, 48, 40, 19, 86]
      		}
      	]
      };
      var pdata = [
      	{
      		value: 300,
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Complete"
      	},
      	{
      		value: 50,
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "In-Progress"
      	}
      ]
      
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);
      
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);
    </script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
  </body>
</html>