 <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <!--<div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">John Doe</p>
          <p class="app-sidebar__user-designation">Frontend Developer</p>
        </div>-->
      </div>


      <ul class="app-menu">
        <li>
		<a class="app-menu__item" href="dashboard.php">
		<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="societyaccstatement.php">
		<i class="app-menu__icon fa fa-plus-square" aria-hidden="true"></i><span class="app-menu__label">Bank Statement</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="admin_profile.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">My Profile</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="mng_user.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Manage Users</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="prepayment.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Add Payment</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="useracc.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">RD Account Details</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="userfdacc.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">FD Account Details</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="userloanacc.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Loan Account Details</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="loanreq.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Loan Requests</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="changepass.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Change Password</span></a>
		</li>
        
		<li>
		<a class="app-menu__item" href="logout.php">
		<i class="app-menu__icon fa fa-sign-out" aria-hidden="true"></i><span class="app-menu__label">Logout</span></a>
		</li>
        
          
        
      </ul>
    </aside>
    