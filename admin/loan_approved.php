<?php include("inc/connect.php"); include("inc/chkAuth.php");

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Loan Approved</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">AR Finance</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
       
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
          
            <li><a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <?php
	include("inc/menu.php");
	?>
	 <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> Loan Approved !</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item"> Loan Approved</li>
        </ul>
      </div>
    <!-- display mesage  -->
	
	  <!-- display msg end -->
	<div class="row">
        <div class="col-md-12">
            <div class="tile">

            	<?php


  $id=$_GET['appID'];
  $dt=date('Y-m-d');

  $sql="update loan_application set status='1',approval_date='$dt' where app_id='$id'";
  
  if(mysqli_query($conn,$sql))
    
    {
      echo "<br><b> Approved !</b><br>";


    /*  $id=$_SESSION['user_id'];
      $loan_amt=$_POST['loan_amt'];
      $tenure=$row['tenure'];
      $interest=18;
      $applydate=$row['apply_date'];
      $approval_date=date('Y-m-d');*/



     
    $cnt="select COUNT(acc_id) from user_account";


if($cnt>0)
{
  $sql="SELECT FLOOR(RAND() * 99999) AS random_num FROM user_account WHERE 'random_num' NOT IN (SELECT acc_no FROM user_account) LIMIT 1";
  $accno=ReturnAnyValue($conn,$sql);
}

else
 {
  $accno=rand(99999,999999);

 }

 $sql="select * from loan_application where app_id=".$_GET['appID'];
 $rs=mysqli_query($conn,$sql);
 $cnt=mysqli_num_rows($rs);

while($row=mysqli_fetch_array($rs))
{
  $id=$row['user_id'];
  $loan_amt=$row['loan_amt'];
  $emi_amt=$row['emi_amt'];
  $tenure=$row['tenure'];
  $interest=$row['interest_rate'];
  $applydate=$row['apply_date'];
  $approval_date=date('Y-m-d');
}
//update statement loan_application acc_no
  
  $sql="insert into user_account(user_id,acc_no,acc_type,loan_amt,emi_amt,interest_rate,start_date,tenure,update_date) values ('$id','$accno','3','$loan_amt','$emi_amt','$interest','$applydate','$tenure','$approval_date')";
  

  if(mysqli_query($conn,$sql))
  {
    
  
  echo "<b>Loan Account Created Successfully !!</b><br>";
  echo "<br> Account Number- "."<b>".$accno."</b>";
    echo "<br> Loan Amount: "."<b>".$loan_amt."</b>";
    echo "<br> Tenure: "."<b>".$tenure." months</b>";
    echo "<br> Rate of interest: "."<b>".$interest."% </b><br>";
    echo "<br> Total EMI Amount: "."<b>".$emi_amt. "</b><br>";

  }
  else
    echo "error:".$sql."<br>".mysqli_error($conn);
  }

  else
    echo "error:".$sql."<br>".mysqli_error($conn);

  $sql="update loan_application set acc_no='$accno' where app_id=".$_GET['appID'];
  mysqli_query($conn,$sql);



?>  
    


</div>
	</div>
	
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
	<script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
    </body>
    </html>
