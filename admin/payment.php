<?php

include("inc/connect.php");
include("inc/chkAuth.php");


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <!-- Twitter meta-->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:site" content="@pratikborsadiya">
    <meta property="twitter:creator" content="@pratikborsadiya">
    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Vali Admin">
    <meta property="og:title" content="Vali - Free Bootstrap 4 admin theme">
    <meta property="og:url" content="http://pratikborsadiya.in/blog/vali-admin">
    <meta property="og:image" content="http://pratikborsadiya.in/blog/vali-admin/hero-social.png">
    <meta property="og:description" content="Vali is a responsive and free admin theme built with Bootstrap 4, SASS and PUG.js. It's fully customizable and modular.">
    <title>Confirm Payment</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body class="app sidebar-mini rtl">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="dashboard.php">AR Finance</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">
        <li class="app-search">
          <input class="app-search__input" type="search" placeholder="Search">
          <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!--Notification Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Show notifications"><i class="fa fa-bell-o fa-lg"></i></a>
          <ul class="app-notification dropdown-menu dropdown-menu-right">
            <li class="app-notification__title">You have 4 new notifications.</li>
            <div class="app-notification__content">
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Lisa sent you a mail</p>
                    <p class="app-notification__meta">2 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Mail server not working</p>
                    <p class="app-notification__meta">5 min ago</p>
                  </div></a></li>
              <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                  <div>
                    <p class="app-notification__message">Transaction complete</p>
                    <p class="app-notification__meta">2 days ago</p>
                  </div></a></li>
              <div class="app-notification__content">
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-primary"></i><i class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Lisa sent you a mail</p>
                      <p class="app-notification__meta">2 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Mail server not working</p>
                      <p class="app-notification__meta">5 min ago</p>
                    </div></a></li>
                <li><a class="app-notification__item" href="javascript:;"><span class="app-notification__icon"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x text-success"></i><i class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                    <div>
                      <p class="app-notification__message">Transaction complete</p>
                      <p class="app-notification__meta">2 days ago</p>
                    </div></a></li>
              </div>
            </div>
            <li class="app-notification__footer"><a href="#">See all notifications.</a></li>
          </ul>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>
            <li><a class="dropdown-item" href="page-user.html"><i class="fa fa-user fa-lg"></i> Profile</a></li>
            <li><a class="dropdown-item" href="page-login.html"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
	<?php
	include("inc/menu.php");
	?>
     <main class="app-content">
      <div class="app-title">
        <div>
          <h1><i class="fa fa-edit"></i> Confirm Payment</h1>
          <p>Confirmation</p>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
          <li class="breadcrumb-item">Confirm Payment</li>
        </ul>
      </div>
      <div class="row">
       <div class="col-md-12">
          <div class="tile">
            <h3 class="tile-title">Confirm Payment</h3>
            <div class="tile-body">
			<?php 
				if(isset($_POST['submit']))
{

  $accno=$_POST['acc_no'];
  $payamt=$_POST['pay_amt'];
  $paymethod=$_POST['pay_method'];
  $paydetail=$_POST['pay_detail'];
  $paydate = date("Y-m-d");
  $prevB=$_POST['prev_bal'];
  $curB=$_POST['cur_bal'];
  $acc_type=$_POST['acc_type'];

  $sql="select COUNT(*) from user_payment where acc_no=$accno";
  $cnt=ReturnAnyValue($conn,$sql);

  if($cnt>0)
  {
    $Ssql="select cur_bal from user_payment where acc_no=$accno order by pay_id DESC LIMIT 0,1";
    $curB=ReturnAnyValue($conn,$Ssql);
    $prevB=$curB;
    $curB=$prevB+$payamt;
  }

  else
  {
    $prevB=0;
    $curB=$prevB+$payamt;

  }

  $sql="insert into user_payment(acc_no,pay_amt,acc_type,pay_method,pay_detail,pay_date,prev_bal,cur_bal) values ('$accno','$payamt','$acc_type','$paymethod','$paydetail','$paydate','$prevB','$curB')";



  if(mysqli_query($conn,$sql))
  {
    
  
   echo " <b>Payment Updated Successfully !!</b>";

   echo "<br> Account Number- ".$accno;
   echo "<br> Current Balance- ".$curB;

   $sql="update user_account set cur_bal=$curB where acc_no=$accno";
   mysqli_query($conn,$sql);



   $Ssql="select cur_bal from society_account order by trans_id DESC LIMIT 0,1";
    $curB=ReturnAnyValue($conn,$Ssql);
    $prevB=$curB;
    $curB=$prevB+$payamt;

$dt=date('Y-m-d');

$login_id=$_SESSION['admin_id'];

$sql="insert into society_account(trans_date,acc_no,trans_amt,trans_type,trans_detail,acc_type,prev_bal,cur_bal,update_date,update_by,status) 
                values ('$dt','$accno','$payamt','1','$paydetail','$acc_type','$prevB','$curB','$dt','$login_id','1')";

mysqli_query($conn,$sql);

  }
  else
    echo "error:".$sql."<br>".mysqli_error($conn);
}
else
{

  $accNO=$_POST['acc_no'];

  $sql="select * from user_account where acc_no=".$accNO;

  $rs=mysqli_query($conn,$sql);
  

  $row=mysqli_fetch_array($rs);

  $cnt=mysqli_num_rows($rs);

  if($cnt==0)
  {
    $msg="Sorry Invalid Account Number !";
    $url="prepayment.php";
     dispMessage($msg,$url);
  }


  $accType=$row['acc_type'];
  if($accType==1)  $amount=$row['fd_amt'];
  if($accType==2)  $amount=$row['monthly_amt'];
  if($accType==3)  $amount=$row['emi_amt'];


  $sql="select name from users where user_id=".$row['user_id'];

  $memName=ReturnAnyValue($conn,$sql);
			?>


  <form   class="form-horizontal" method="post" name="myform" action="" enctype="multipart/form-data"> 
  
  <input type="hidden" name="acc_no" id="acc_no" value=<?php echo $accNO;?>>
  <input type="hidden" name="acc_type" id="acc_type" value=<?php echo $accType;?>>
  <input type="hidden" name="pay_amt" id="pay_amt" value=<?php echo $amount;?>>

  
  <div class ="form-group row">
    <label class="control-label col-md-3" for="acc_type">Account Type:  
                          <?php  if($accType==1) echo "<b>FD Account</b>";
                          if($accType==2) echo "<b>RD Account</b>";
                          if($accType==3) echo "<b>Loan Account</b>"; ?> </label>
      <div class="col-md-8">
      </div>
    </div>


  <div class="form-group row">
    Account Number:  <?php echo "<b>".$accNO."</b>"; ?>
    <div class="col-md-8">
      </div>
  </div>

   <div class="form-group row">
   Member Name:  <?php echo "<b>".$memName."</b>";?>
   <div class="col-md-8">
      </div>
  </div>


  <div class="form-group row">
    Payment Amount: <?php echo "<b>".$amount."</b>"; ?>
    <div class="col-md-8">
      </div>
  </div>

 

<div class="form-group row">
    <label for="pay_method" class="control-label col-md-3">Payment Method</label>
    <div class="col-md-8">
  <select  name="pay_method" class="form-group" id="pay_method">
    <option value="">-- Select Category --</option>
    <option value="1">Cash</option>
    <option value="2">Cheque</option>
    <option value="3">Online Transfer</option>
  </select>
  </div>
</div>

<div class="form-group row">
    <label class="control-label col-md-3" for="pay_detail">Payment Detail</label>
    <div class="col-md-8">
  <textarea class="form-control" name="pay_detail" rows="4" id="pay_detail" required></textarea>
</div>
</div>

				
				
                
                <!--<div class="form-group row">
                  <label class="control-label col-md-3">Item Image</label>
                  <div class="col-md-8">
                    <input class="form-control" type="file" name="file">
                  </div>
                </div>-->
                
				
				<div class="form-group row">
                  <div class="col-md-8 col-md-offset-9">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="submit" name="submit" value="Pay" class="btn btn-primary">
                      </label>
                    </div>
                  </div>
                </div>
				
				<?php
      }
      ?>
				
              </form>
            </div>
            
          </div>
        </div>
        <div class="clearix"></div>
      </div>
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <!-- Page specific javascripts-->
    <!-- Google analytics script-->
    <script type="text/javascript">
      if(document.location.hostname == 'pratikborsadiya.in') {
      	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      	ga('create', 'UA-72504830-1', 'auto');
      	ga('send', 'pageview');
      }
    </script>
	<script>
$(document).ready(function(){
	
	$("#ordQty").focusout(function(){
    var oQty=parseInt($(this).val());
	var aQty=parseInt($("#avail_qty").val());
	
	//$("#ordQty").val(0);
	if(oQty > aQty)
	{ alert("Sorry, order qty is more than available qty");
	  $(this).val(0);
	   $(this).focus();
	  }
	
  });
		
  $("#category").change(function() {
	  var catid= $(this).val() ;
	 
	  $.ajax({
	  type: 'post',
	  url: 'load_products.php',
	  data: {
	   catID:catid
	   },
     success: function(response){
     
	    $( '#listProd' ).html(response);
     }
   });
  });
  
    $(document.body).on('change', '#prodID', function() {
		
	  var pid= $(this).val() ;
	 
	  $.ajax({
	  type: 'post',
	  url: 'load_product_details.php',
	  data: {
	   prodID:pid
	   },
     success: function(response){
     
	    $( '#pDetails' ).html(response);
     }
   });
  });
  
});
	
	</script
  </body>
</html>