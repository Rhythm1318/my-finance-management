 <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">


      <!--<div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">John Doe</p>
          <p class="app-sidebar__user-designation">Frontend Developer</p>
        </div>-->
      </div>
      <ul class="app-menu">
        <li>
		<a class="app-menu__item" href="dashboard.php">
		<i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="user_profile.php">
		<i class="app-menu__icon fa fa-plus-square" aria-hidden="true"></i><span class="app-menu__label">My Profile</span></a>
		</li>

		<?php 
		if($_SESSION['rd_account_no']=="") 
			{ ?>
			<li>
			<a class="app-menu__item" href="create_acc.php">
			<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">My RD Account</span></a>
			</li>


		<?php
			}
		else
		{
		?>
			<li>
			<a class="app-menu__item" href="listrd.php">
			<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">My RD Account</span></a>
			</li>
		<?php
		}
		?>	

		<?php 
		if($_SESSION['fd_account_no']=="") 
			{ ?>
			<li>
			<a class="app-menu__item" href="create_fd_acc.php">
			<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">My FD Account</span></a>
			</li>


		<?php
			}
		else
		{
		?>
			<li>
			<a class="app-menu__item" href="listfd.php">
			<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">My FD Account</span></a>
			</li>
		<?php
		}
		?>


		<?php 
		if($_SESSION['loan_account_no']!="") 
			{ ?>
			<li>
			<a class="app-menu__item" href="listloan.php">
			<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">My Loan Account</span></a>
			</li>
		<?php
			}?>
		
		<li>
		<a class="app-menu__item" href="loanreq.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Request Loan</span></a>
		</li>
			



		<li>
		<a class="app-menu__item" href="loanreq_status.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Track Loan</span></a>
		</li>
		<li>
		<a class="app-menu__item" href="changepass.php">
		<i class="app-menu__icon fa fa-plus-circle" aria-hidden="true"></i><span class="app-menu__label">Change Password</span></a>
		</li>
        
		<li>
		<a class="app-menu__item" href="logout.php">
		<i class="app-menu__icon fa fa-sign-out" aria-hidden="true"></i><span class="app-menu__label">Logout</span></a>
		</li>
        
          
        
      </ul>
    </aside>
    